package com.example.jsonparcer


data class jsonParcer (
     var id: String?,
     var projectId:String?,
     var equipmentId:String?,
     var status:String?,
     var requestedBy:String?,
     var acceptedBy:String?,
     var author:String?,
     var category:String?,
     var locations:location?,
     var filters:List<filter_type>,

     var type:String?,
     var organization:String?,
     var address:String?,
     var startDate:String?,
     var endDate:String?,
     var description:String?,
     var prolongDates:List<String>?,
     var releaseDates:List<String>?,
     var isDummy:Boolean?,
     var hasDriver:Boolean?,
     var overwriteDate:String?,
     var metaInfo:String?,
     var warehouseId:String?,
     var rentalDescription:String?,
     var internalTransportations:interTransportations?,
     var startDateMilliseconds:Long?,
     var endDateMilliseconds:Long?,
     var equipment: equipmentData?,
)
{   data class interTransportations(
    val id:String?,
    var projectRequestId:String?,
    var pickUpDate:String?,
    var deliveryDate:String?,
    var description:String?,
    var status:String?,
    var startDateOption:String?,
    var endDateOption:String?,
    var pickUpLocation : location?,
    var deliveryLocation : location?,
    var provider:String?,
    var pickUpLocationAddress:String?,
    var deliveryLocationAddress:String?,
    var pGroup:String?,
    var isOrganizedWithoutSam:String?,
    var templatePGroup:String?,
    var pickUpDateMilliseconds:String?,
    var deliveryDateMilliseconds:String?,
    var startDateOptionMilliseconds:Double?,
    var endDateOptionMilliseconds:Double?,
)
    data class equipmentData(
        var id:String?,
        var title: String?,
        var invNumber:String?,
        var categoryId:String?,
        var modelId:String?,
        var brandId:String?,
        var year:Int?,
        var specifications:List<data>,
        var weight:Int?,
        var additional_specifications:String?,
        var structureId:String?,
        var organizationId:String?,
        var beaconType:String?,
        var beaconId:String?,
        var beaconVendor:String?,
        var RFID:String?,
        var dailyPrice:String?,
        var inactive:String?,
        var tag:threeOf?,
        var telematicBox:String?,
        var createdAt:String?,
        var special_number:String?,
        var last_check:String?,
        var next_check:String?,
        var responsible_person:String?,
        var test_type:String?,
        var unique_equipment_id:String?,
        var bgl_number:String?,
        var serial_number:String?,
        var inventory:String?,
        var warehouseId:String?,
        var trackingTag:String?,
        var workingHours:String?,
        var navaris_criteria:String?,
        var dont_send_to_as400:Boolean?,
        var model:model?,
        var brand:brand?,
        var category:categoryfive?,
        var structure:structureFour?,
        var wareHouse:String?,
        var equipmentMedia:List<equipData>?,
        var telematics:telematicData?,
        var isMoving:Boolean?

    )
    data class telematicData(
        var timestamp:Long?,
        var eventType:String?,
        var projectId:String?,
        var equipmentId:String?,
        var locationName:String?,
        var location:locationData?,

    )
    data class locationData(
        var type:String?,
        var coordinates: List<List<List<List<Double>>>>
    )
    data class equipData(
        var id:String?,
        var name:String?,
        var files:List<fileData>?,
        var type:String?,
        var modelId:String?,
        var main:Boolean?,
        var modelType:String?,
        var createdAt:String?
    )
    data class fileData(
        var size:String?,
        var path:String?
    )
    data class structureFour(
        var id: String?,
        var name: String?,
        var type: String?,
        var color: String?
    )
    data class categoryfive(
        var id: String?,
        var name: String?,
        var name_de: String?,
        var createdAt: String?,
        var media: List<String>
    )
    data class model(
        var id:String?,
        var name:String?,
        var createdAt:String?,
        var brand:brand?,

    )
    data class brand(
        var id:String?,
        var name:String?,
        var createdAt:String?,

    )
    data class threeOf(
        var date:String?,
        var authorName:String?,
        var media:List<String>
                )
    data class data(
        var key:String?,
        var value:Any?
    )
    data class location(
        var type:String?,
        var coordinates : List<Double>
    )
    data class filter_type(
        var name:String,
        var value:value
        )
    data class  value(
        var max:String?,
        var min:String?
        )
}
